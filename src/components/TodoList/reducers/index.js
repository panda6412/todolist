const initialState = { id: "", text: "", todoList: [] };

function TodoHeaderReducer(state = initialState, action) {
  const { type, id, text } = action;
  const { todoList } = state;

  switch (type) {
    case "TODO_VALUE_CHANGE":
      return { ...state, text };
    case "ADD_TODO":
      if (!text) {
        return { ...state };
      }
      todoList.push({ id, text });
      return { ...state, todoList };
    case "DELETE_TODO":
      console.log(`trynna splice array >>> ${todoList} , index >>> ${id}`);
      todoList.splice(id, 1);
      return { ...state, todoList };
    default:
      return { ...state };
  }
}

export default TodoHeaderReducer;
