let nextTodoId = 0;

export const todoValueChange = text => {
  return { type: "TODO_VALUE_CHANGE", text };
};

export const addTodo = text => {
  return { type: "ADD_TODO", id: nextTodoId++, text };
};

export const deleteTodo = id => {
  return { type: "DELETE_TODO", id };
};
