import React, { Component } from "react";
import { Input, Icon, Table, Button } from "semantic-ui-react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as defaultActions from "../actions";

class TodoListComponent extends Component {
  componentDidMount() {}
  render() {
    const { dispatch, actions, state } = this.props;
    console.log(state);
    return (
      <div>
        <Input
          placeholder="Add todo here .."
          value={state.text}
          onChange={e => {
            // dispatch(defaultActions.todoValueChange(e.target.value));
            actions.todoValueChange(e.target.value);
          }}
        />
        <Icon
          circular
          name="plus"
          style={{ marginLeft: "0.5em" }}
          onClick={() => {
            actions.addTodo(state.text);
            actions.todoValueChange("");
          }}
        />
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Id</Table.HeaderCell>
              <Table.HeaderCell>text</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {state.todoList.map((data, index) => {
              return (
                <Table.Row key={index}>
                  <Table.Cell>{index}</Table.Cell>
                  <Table.Cell>
                    {data.text}
                    <Button
                      floated="right"
                      icon
                      size="small"
                      onClick={() => {
                        actions.deleteTodo(index);
                      }}
                    >
                      <Icon name="cancel" />
                    </Button>
                  </Table.Cell>
                </Table.Row>
              );
            })}
          </Table.Body>
        </Table>
      </div>
    );
  }
}

// let this container could use dispatch through the propertites
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(defaultActions, dispatch),
  dispatch
});

// let this container could access state through the propertites
const mapStateToProps = state => {
  return { state };
};

// decorator , let this class more powerful, could access the state and dispatch from the store .
TodoListComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoListComponent);

export default TodoListComponent;
