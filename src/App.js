import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import TodoListComponent from "./components/TodoList";
import { Header } from "semantic-ui-react";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header as="h1">Todo List Tool</Header>
        <TodoListComponent />
      </div>
    );
  }
}

export default App;
